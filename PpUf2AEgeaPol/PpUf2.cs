using System;

namespace PpUf2AEgeaPol
{
    public static class ProvaPractica
    {
        public static void Main()
        {
            
        }
        public static double pi(int delimitador)
        {
            double valorpi = 0;
            for(int j=delimitador, valordivisio=1,negatiu=0;j>0;j-- ,valordivisio+=2)
            {
                if (negatiu == 1)
                {
                    valorpi -= (10000000000000000 / valordivisio * 10000000000000000);
                    negatiu--;
                }
                else if (negatiu==0) {
                    valorpi += (10000000000000000 / valordivisio * 10000000000000000);
                    negatiu++;
                }
            }

            return valorpi;
        }

        public static int potencia(int valor,int valor2, int comptador)
        {
            if (comptador == 1) return valor;
            if (comptador == 0) return 1;
            return potencia(valor*valor2, valor2 ,comptador - 1);
        }

        public static string Vector(int[] vector1,int numero)
        {
            string resultat="";
            for (int j=0;j<vector1.Length;j++)
            {
                if (vector1[j] == numero) resultat += " " + Convert.ToString(j) + " ";
            }
            if (resultat =="") return "No hi ha el número a buscar";
            return "Posicions; "+resultat;
        }
    }
}