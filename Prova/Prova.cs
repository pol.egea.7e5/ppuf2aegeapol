using System;
using Dades;
using PpUf2AEgeaPol;

namespace Prova
{
    /// <summary>
    /// Classe per al menú.
    /// </summary>
    internal class MenuProva
    {
        /// <summary>
        /// Mètode Per a mostrar el menú mentres no posin fi.
        /// </summary>
        static void Main()
        {
            do
            {
                MostraMenu();
            }
            while (!ExecutaOpcio()) ;
        }

        public static void MostraMenu()
        {
            Console.WriteLine("1-PI");
            Console.WriteLine("2-Vector");
            Console.WriteLine("3-Potencia de x");
            Console.WriteLine("4-FI");

        }

        public static bool ExecutaOpcio()
        {
            string opcio = Console.ReadLine();
            switch (opcio)
            {
                case "1":
                    Pi();
                    break;
                case "2":
                    Vector();
                    break;
                case "3":
                    Xfuncio();
                    break;
                case "4":
                    return true;
            }
            return false;
        }

        public static void Pi()
        {
            int delimitador = EntrarDades.piDelimitador();
            Console.WriteLine(ProvaPractica.pi(delimitador));
            
        }

        public static void Xfuncio()
        {
            int valorx=EntrarDades.xReal();
            int valorp = EntrarDades.potencia();
            Console.WriteLine(ProvaPractica.potencia(valorx,valorx,valorp));
        }

        public static void Vector()
        {
            Console.WriteLine("Tamany del vector(mínim 1)");
            int valorMax = EntrarDades.ValorMax();
            int [] vector1 = EntrarDades.Vector(EntrarDades.Tamany(),EntrarDades.ValorMin(valorMax),valorMax);
            int numeroBusca = EntrarDades.EnterBuscar();
            Console.WriteLine(ProvaPractica.Vector(vector1,numeroBusca));
        }
    }
}