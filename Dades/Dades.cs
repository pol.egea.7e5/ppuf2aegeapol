using System;

namespace Dades
{
    /// <summary>
    /// Classe per a les entrades de dades.
    /// </summary>
    public class EntrarDades
    {
        /// <summary>
        /// Mètode obligatori per a l'execució.
        /// </summary>
        public static void Main()
        {
            
        }
        /// <summary>
        /// Mètode d'entrada d'enter
        /// </summary>
        /// <returns>Retorna enter</returns>
        public static int piDelimitador()
        {
            Console.WriteLine("Introdueix el nombre de termes a calcular");
            return Convert.ToInt32(Console.ReadLine());
        }

        public static int potencia()
        {
            int valorX;
            Console.WriteLine("Introdueix número sencer");
            do valorX = Convert.ToInt32(Console.ReadLine());
            while (valorX < 0);
            return valorX;
        }
        public static int xReal()
        {
            Console.WriteLine("Introdueix valor de X");
            return Convert.ToInt32(Console.ReadLine());
        }
        public static int[] Vector(int tamany, int valorMin, int valorMax)
        {
            var rnd = new Random();
            int[] vector1 = new int[tamany];
            Console.WriteLine("Dades vector:");
            for (int i = 0; i < tamany; i++)
            {
                vector1[i] = rnd.Next(valorMin, valorMax+1);
                Console.Write($" {vector1[i]} ");
            }
            return vector1;
        }
        public static int ValorMax()
        {
            Console.WriteLine("Introdueix valor Màxim");
            return Convert.ToInt32(Console.ReadLine());
        }
        public static int ValorMin(int valorMax)
        {
            int valorX;
            Console.WriteLine("Introdueix valor mínim sent mes petit que el valor introduit previament");
            do valorX = Convert.ToInt32(Console.ReadLine());
            while (valorX >= valorMax);
            return valorX;
        }
        public static int Tamany()
        {
            int valorX;
            Console.WriteLine("Introdueix tamany més gran de 0");
            do valorX = Convert.ToInt32(Console.ReadLine());
            while (valorX < 0);
            return valorX;
        }
        public static int EnterBuscar()
        {
            Console.WriteLine("Introdueix el numero a buscar");
            return Convert.ToInt32(Console.ReadLine());
        }
    }
}